using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Back2Front.GUI
{
	public class EntryElementAdv : EntryElement
	{
		private UIColor _backgroundColor;
		private UIColor _textColor;
		public EntryElementAdv(string caption, string placeholder, string value) : this(caption,placeholder,value,UIColor.Black, UIColor.White)
		{

		}

		public EntryElementAdv(string caption, string placeholder, string value, UIColor textColor,  UIColor backgroundColor) : base(caption,placeholder,value)
		{
			_backgroundColor = backgroundColor;
			_textColor = textColor;
		}

		public override UITableViewCell GetCell(UITableView tv)
		{
			var cell= base.GetCell(tv);
			cell.BackgroundColor = _backgroundColor;
			cell.TextLabel.TextColor = _textColor;
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			((UITextField)cell.ContentView.Subviews [1]).TextAlignment = UITextAlignment.Right;
			((UITextField)cell.ContentView.Subviews [1]).TextColor = _textColor;
			return cell;
		}

		public void SetValueAndUpdate(string value)
		{
			Value = value;
			if(GetContainerTableView () != null)
			{
				var root = GetImmediateRootElement();
				root.Reload (this, UITableViewRowAnimation.None);
			}
		}
	}
}

