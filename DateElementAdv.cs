using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Back2Front.GUI
{
	public class DateElementAdv : DateElement
	{
		public DateElementAdv (string caption, DateTime dateTime, UIImage image) : base(caption, dateTime)
		{
			this.BackgroundColor = UIColor.FromPatternImage(image);
		}
	}

	public class TimeElementAdv : TimeElement
	{
		private UIColor _backColor;
		private UIColor _textColor;
		private int _minuteInterval;
		public event EventHandler<DateTime> DateChanged;

		public TimeSpan OriginalTime{ get; set; }

		public TimeElementAdv (string caption, DateTime dateTime, UIColor backColor, UIColor textColor, UIImage backgroundImage, int minuteInterval) : base(caption, dateTime)
		{
			OriginalTime = dateTime.TimeOfDay;
			_backColor = backColor;
			_textColor = textColor;
			BackgroundColor = UIColor.FromPatternImage(backgroundImage);
			_minuteInterval = minuteInterval;
			DateSelected += (obj) => {

				if (DateChanged != null) {
					DateChanged (this, obj.DateValue);
				}
			};
		}

		public override UITableViewCell GetCell(UITableView tv)
		{
			var cell= base.GetCell(tv);
			cell.BackgroundColor = _backColor;
			cell.TextLabel.TextColor = _textColor;
			cell.DetailTextLabel.TextColor = _textColor;
			return cell;
		}

		public override UIDatePicker CreatePicker ()
		{
			var picker = base.CreatePicker ();
			picker.MinuteInterval = _minuteInterval;
			picker.BackgroundColor = UIColor.White.ColorWithAlpha(0.5f);
			return picker;
		}
	}
}

