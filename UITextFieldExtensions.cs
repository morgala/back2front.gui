using System;
using System.Drawing;
using MonoTouch.AudioToolbox;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;

namespace Back2Front.GUI
{
	public static class UITextFieldExtensions
	{
		public static void SetChromeStyle(this UITextField textField, UIColor borderColour)
		{
			textField.BorderStyle = UITextBorderStyle.Line;
			textField.BackgroundColor = UIColor.White;
			textField.TextColor = UIColor.DarkGray;


			//textField.text
			//textField.Layer.MasksToBounds = false;	
			//textField.Layer.ShadowOffset = new SizeF(100f,200f);
			//textField.Layer.ShadowOpacity = 1f;
			//textField.Layer.ShadowRadius = 6f;
			//textField.Layer.ShadowColor = UIColor.LightGray.CGColor;
			//textField.Layer.CornerRadius = 3f;
			textField.Layer.BorderWidth = 1;
			textField.Layer.BorderColor = borderColour.CGColor;

		}

		public static void Wobble(this UITextField control)
		{
			if (control.IsFirstResponder) {
				control.ResignFirstResponder ();
			}
			CABasicAnimation animation = CABasicAnimation.FromKeyPath("transform.translation.x");
			animation.RepeatDuration = 1;
			animation.From = NSNumber.FromFloat(1.5f);
			animation.To = NSNumber.FromFloat(-1.5f);
			animation.Speed = 2.5f;
			control.Layer.AddAnimation (animation, "animateTextBox");
			SystemSound.Vibrate.PlaySystemSound();
		}
	}
}

