﻿using System;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.Dialog;
using MonoTouch.UIKit;

namespace Back2Front.GUI
{	
	public class ImageButtonElementArgs : EventArgs{
		public ImageButtonElementArgs(DialogViewController dvc, UITableView tableView, NSIndexPath path)
		{
			DialogViewController = dvc;
			TableView = tableView;
			Path = path;
		}
		public DialogViewController DialogViewController{ get; private set; }
		public UITableView TableView{ get; private set; }
		public NSIndexPath Path{ get; private set; }
	}

	public class ImageButtonElement : Element
	{
		private UIImage _offImage;
		private UIImage _onImage;
		private string _caption;
		public event EventHandler<ImageButtonElementArgs> ItemSelected;


		public bool MarkAsSelected{ get; set; }
		public DataCell DataCellContainer{ get; private set; }

		public ImageButtonElement (string caption, UIImage offImage, UIImage onImage) : this (caption, offImage, onImage, false)
		{
		}

		public ImageButtonElement (string caption, UIImage offImage, UIImage onImage, bool markAsSelected) : base (string.Empty)
		{
			_caption = caption;
			_offImage = offImage;
			_onImage = onImage;
			MarkAsSelected = markAsSelected;
		}

		public void Refresh()
		{
			DataCellContainer.Update (MarkAsSelected);
		}


		public override MonoTouch.UIKit.UITableViewCell GetCell (MonoTouch.UIKit.UITableView tv)
		{
			DataCell ret = tv.DequeueReusableCell ("cellxid ") as DataCell;
			if (ret == null) {
				ret = new DataCell (_offImage, _onImage, _caption,  "cellxid", MarkAsSelected);
				ret.BackgroundColor = UIColor.Clear;
				tv.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
				ret.SelectionStyle = UITableViewCellSelectionStyle.None;
			}
			DataCellContainer = ret;
			return ret;
		}

		public override void Selected (DialogViewController dvc, UITableView tableView, MonoTouch.Foundation.NSIndexPath path)
		{
			base.Selected (dvc, tableView, path);
			var cell = (DataCell) tableView.CellAt (path);
			cell.Update (cell.Selected);

			if (ItemSelected != null) {
				ItemSelected (this, new ImageButtonElementArgs (dvc, tableView, path));
			}
		}

		public override void Deselected (DialogViewController dvc, UITableView tableView, MonoTouch.Foundation.NSIndexPath path)
		{
			base.Deselected (dvc, tableView, path);
			var cell = (DataCell) tableView.CellAt (path);
			cell.Update (cell.Selected);
		}

		public class DataView : UIView
		{
			private UIImage _imageOff;
			private UIImage _imageOn;
			private string _text;
			private bool _isSelected;
			private UIColor _selectedColor =  UIColor.FromRGB (80, 169, 237);

			public DataView (UIImage imageOff, UIImage imageOn, string text, bool markAsSelected)
			{
				this.BackgroundColor = UIColor.Clear;
				Initialise (imageOff, imageOn, text, markAsSelected);
			}

			public void Initialise (UIImage imageOff, UIImage imageOn, string text, bool markAsSelected)
			{
				_imageOff = imageOff;
				_imageOn = imageOn;
				_text = text;
				_isSelected = markAsSelected;
				SetNeedsDisplay ();
			}

			public void Update(bool isSelected)
			{
				_isSelected = isSelected;
				SetNeedsDisplay ();
			}

			public override void Draw (RectangleF rect)
			{
				if (_isSelected) {
					_imageOn.Draw (new PointF (20, 7));
					_selectedColor.SetColor ();
				}
				else
				{
					_imageOff.Draw (new PointF (20, 7));
					UIColor.White.SetColor ();
				}
				DrawString (_text, new PointF (80, 12), UIFont.SystemFontOfSize (18));
			}
		}

		public class DataCell : UITableViewCell
		{
			private DataView _dataView;

			public DataCell (UIImage imageOff, UIImage imageOn, string text, string identKey, bool markAsSelected) : base (UITableViewCellStyle.Default, identKey)
			{
				_dataView = new DataView (imageOff, imageOn, text, markAsSelected);
				ContentView.Add (_dataView);
			}

			public void Update(bool selected)
			{
				_dataView.Update (selected);
			}


			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();
				_dataView.Frame = ContentView.Bounds;
				_dataView.SetNeedsDisplay ();
			}
		}
	}


}

