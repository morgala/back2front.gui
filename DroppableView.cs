﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreAnimation;

namespace Back2Front.GUI
{
	public class TargetViewArgs : EventArgs
	{
		public TargetViewArgs(UIView view)
		{
			View = view;
		}
		public UIView View{ get; private set; }
	}

	public class DroppableView : UIView
	{
		private bool _shouldUpdateReturnPosition;
		private bool _isDragging;
		private bool _didInitaliseReturnPosition;
		private PointF _returnPosition;
		private UIScrollView _scrollView;
		private UIView _outerView;
		private UIView _dropTarget;
		private UIView _activeDropTarget;
		//private Func<bool> _shouldAnimateViewBack;

		public event EventHandler<EventArgs> BeganDragging;
//		public event EventHandler<TargetViewArgs> EnteredTarget;
//		public event EventHandler<TargetViewArgs> OnTarget;

		public DroppableView (RectangleF frame, UIView target) : base(frame)
		{
			_dropTarget = target;
			Initialise ();
		}

		public DroppableView (UIView target):base()
		{
			_dropTarget = target;
			Initialise ();
		}

		public override void TouchesBegan (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			BeginDrag ();
			DragAtPosition ((UITouch) touches.AnyObject);
		}

		public override void TouchesMoved (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			DragAtPosition ((UITouch) touches.AnyObject);
		}

		public override void TouchesEnded (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			EndDrag ();
		}

		public override void TouchesCancelled (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
			EndDrag ();
		}

		public void LeftTarget(UIView target)
		{
			target.Transform = CGAffineTransform.MakeScale (1.0f, 1.0f);
			//target.BackgroundColor = UIColor.Orange;
		}

		public override void WillMoveToSuperview (UIView newsuper)
		{
			if (!_isDragging && newsuper is UIScrollView) {
				_scrollView = (UIScrollView)newsuper;
				_outerView = _scrollView.Superview;
			}
		}

		private void Initialise()
		{
			_shouldUpdateReturnPosition = true;
		}


		private void BeginDrag()
		{
			_isDragging = true;

			if(BeganDragging != null)
			{
				BeganDragging(this, null);
			}

			UIView.Animate (0.33f, () => {
				this.BackgroundColor = UIColor.FromRGBA(1f,0.5f, 0f, 1f);
				this.Alpha = 0.8f;
			});

			if (!_didInitaliseReturnPosition || _shouldUpdateReturnPosition) {
				_returnPosition = Center;
				_didInitaliseReturnPosition = true;
			}

			ChangeSuperView ();
		}

		private void DragAtPosition(UITouch touch)
		{
			// animate into new position
			UIView.Animate (0.33f, () => {
				Center = touch.LocationInView(Superview);
			});

			if (_dropTarget != null)
			{
				UIView rootView = Superview.Superview;;
				RectangleF viewRect = ConvertRectToView (Bounds, rootView);

				RectangleF dropTargetRect = _dropTarget.ConvertRectToView (_dropTarget.Bounds, rootView);
				RectangleF intersect = RectangleF.Intersect (viewRect, dropTargetRect);
				bool didHitTarget = intersect.Size.Width > 10 || intersect.Size.Height > 10;

				if (didHitTarget)
				{
					if (_activeDropTarget != _dropTarget) {

						if (_activeDropTarget != null) {
							_activeDropTarget.Transform = CGAffineTransform.MakeScale (1.0f, 1.0f);
							_activeDropTarget.BackgroundColor = UIColor.Orange;
						}
						_activeDropTarget = _dropTarget;
						_activeDropTarget.Transform = CGAffineTransform.MakeScale (1.5f, 1.5f);
						_activeDropTarget.BackgroundColor = UIColor.Green;

					}
				}
				else
				{
					if (_activeDropTarget == _dropTarget) {
						_activeDropTarget.Transform = CGAffineTransform.MakeScale (1.0f, 1.0f);
						_activeDropTarget.BackgroundColor = UIColor.Orange;
					}
					_activeDropTarget = null;
				}
			}
		}

		private void EndDrag()
		{
			UIView.Animate (0.33f, () => {
				if(_activeDropTarget != null)
				{
					BackgroundColor = UIColor.Black;
				}
				else{
					BackgroundColor = UIColor.DarkGray;
				}
			});

			bool shouldAnimateBack = _activeDropTarget == null;
			if (shouldAnimateBack) {
				ChangeSuperView ();
			}

			_isDragging = false;
			_activeDropTarget = null;
			if (shouldAnimateBack) {
				UIView.Animate (0.33f, () => {
					Center = _returnPosition;
				});
			}
		}

		private void ChangeSuperView()
		{
			if (_scrollView != null) {
				Superview.BringSubviewToFront (this);
				return;
			}

			UIView tmp = Superview;
			RemoveFromSuperview ();
			_outerView.AddSubview (this);
			_outerView = tmp;

			PointF centre = Center;
			if (_outerView == _scrollView) {
				centre.X += _scrollView.Frame.Location.X - _scrollView.ContentOffset.X;
				centre.Y += _scrollView.Frame.Location.Y - _scrollView.ContentOffset.Y;
			} else {
				centre.X -= _scrollView.Frame.Location.X - _scrollView.ContentOffset.X;
				centre.Y -= _scrollView.Frame.Location.Y - _scrollView.ContentOffset.Y;
			}
			Center = centre;
		}
	}
}

