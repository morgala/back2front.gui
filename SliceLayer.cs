﻿using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreAnimation;
using MonoTouch.CoreGraphics;

namespace Back2Front.GUI
{
	public class SliceLayer : CAShapeLayer
	{
		private CABasicAnimation _animation;

		public SliceLayer ()
		{
		}

		public SliceLayer (IntPtr handle) : base(handle)
		{
		}

		public SliceLayer(SliceLayer layer)
		{
			StartAngle = layer.StartAngle;
			EndAngle = layer.EndAngle;
		}

		public double StartAngle{get;set;}
		public double EndAngle{get;set;}
		public float Percentage{get;set;}
		public float Value{ get; set; }
		public bool IsSelected{ get; set; }

		public void CreateArcAnimationForKey(string key, float fromValue, float toValue, Action<CAAnimation> animStarted, Action<CAAnimation,bool> animStopped)
		{
			NSString stringKey = new NSString (key);
			_animation = CABasicAnimation.FromKeyPath (key);
			_animation.Delegate = new SliceLayerDelegate(animStarted, animStopped);
			NSObject currentAngle = null;
			if (PresentationLayer != null) {
				currentAngle = PresentationLayer.ValueForKey (stringKey);
			}
			if (currentAngle == null) {
				currentAngle = NSNumber.FromFloat (fromValue);
			}
			_animation.From = currentAngle;
			_animation.To = NSNumber.FromFloat (toValue);
			_animation.TimingFunction = CAMediaTimingFunction.FromName (CAMediaTimingFunction.Default);
			AddAnimation (_animation, key);
			SetValueForKey (NSNumber.FromFloat (toValue), stringKey);
		}

		internal class SliceLayerDelegate : CAAnimationDelegate
		{
			private Action<CAAnimation> _animStarted;
			private Action<CAAnimation, bool> _animStopped;

			public SliceLayerDelegate(Action<CAAnimation> animStarted, Action<CAAnimation,bool> animStopped)
			{
				_animStarted = animStarted;
				_animStopped = animStopped;
			}

			public override void AnimationStarted(CAAnimation anim)
			{
				_animStarted (anim);
			}

			public override void AnimationStopped (CAAnimation anim, bool finished)
			{
				_animStopped (anim, finished);
			}
		}
	}
}

