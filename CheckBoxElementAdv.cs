﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;


namespace Back2Front.GUI
{
	public class CheckBoxElementAdv : CheckboxElement
	{
		public event EventHandler<CheckBoxSelectedArgs> OnSelected;

		public CheckBoxElementAdv (string caption, bool state, string group) : base(caption, state, group)
		{
		}
			
		public override void Selected (DialogViewController dvc, MonoTouch.UIKit.UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			base.Selected (dvc, tableView, indexPath);
			if (OnSelected != null){
				OnSelected (this, new CheckBoxSelectedArgs(Value));
			}
		}

		public override UITableViewCell GetCell(UITableView tv)
		{
			var cell= base.GetCell(tv);
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;
			return cell;
		}
	}

	public class CheckBoxSelectedArgs : EventArgs{
		public CheckBoxSelectedArgs(bool selected)
		{
			Selected = selected;
		}

		public bool Selected{ get; private set; }
	}
}

