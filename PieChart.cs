using System;
using System.Collections.Generic;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;

namespace Back2Front.GUI
{
	public class PieChart
	{
		public static UIImage Create(int width, int height, List<Slice> slices)
		{
			//Create the CGBitmapContext object
			CGBitmapContext ctx = new CGBitmapContext (IntPtr.Zero, width, height, 8, 4 * width, CGColorSpace.CreateDeviceRGB(), CGImageAlphaInfo.PremultipliedFirst);
			CGColor blackBorder = UIColor.Black.CGColor;

			//Calculate the arc start points, center of the circle
			float x = width / 2;
			float y = height / 2;

			//Set the radius
			float radius = x;

			//There are 2 pi radians in a full circle
			float twopi = (2f * (float)Math.PI) ;

			// first draw outer rim
			CGPath rimPath = new CGPath();
			rimPath.AddArc(x, y, radius, 0, twopi, false);
			ctx.SetFillColor (blackBorder);
			ctx.AddPath (rimPath);
			ctx.DrawPath (CGPathDrawingMode.Fill);

			float startAngle = 0f;
			float endAngle = 0f;
			foreach(Slice s in slices)
			{
				endAngle += s.Percentage * twopi;
				CGPath path = new CGPath();
				path.AddArc (x, y, radius-1, startAngle, endAngle, false);
				path.AddLineToPoint(x,y);
				startAngle = endAngle;
				ctx.SetFillColor (s.Color);
				ctx.AddPath (path);
				ctx.DrawPath (CGPathDrawingMode.Fill);
			}
			return UIImage.FromImage (ctx.ToImage ());
		}
	}

	public class Slice
	{
		public string Name{get;set;}
		public float Percentage{get;set;}
		public CGColor Color{get;set;}	
	}
}

