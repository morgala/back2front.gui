using System;
using MonoTouch.UIKit;

namespace Back2Front.GUI
{
	public static class SystemFont
	{
		private static Version _version;
		private enum Version
		{
			IOS7,
			Other
		}

		static SystemFont()
		{
			if (UIDevice.CurrentDevice.SystemVersion.StartsWith("7"))
			{
				_version = Version.IOS7;
			}
			else
			{
				_version = Version.Other;
			}
		}

		public static UIFont SystemFontOfSize(float size)
		{
			if (_version == Version.IOS7)
			{
				return UIFont.FromName ("HelveticaNeue-UltraLight", size);
			}
			return UIFont.SystemFontOfSize (size);
		}

		public static UIFont BoldSystemFontOfSize(float size)
		{
			if (_version == Version.IOS7)
			{
				return UIFont.FromName ("HelveticaNeue-Medium", size);
			}
			return UIFont.BoldSystemFontOfSize (size);
		}

		public static UIFont ItalicSystemFontOfSize(float size)
		{
			if (_version == Version.IOS7)
			{
				return UIFont.FromName ("HelveticaNeue-UltraLightItalic", size);
			}
			return UIFont.ItalicSystemFontOfSize (size);
		}

	}
}

