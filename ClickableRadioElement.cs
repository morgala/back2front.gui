using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Back2Front.GUI
{
	public class ClickableRadioElement : RadioElement
	{
		public event EventHandler<EventArgs> OnSelected;

		public ClickableRadioElement (string caption) : base(caption)
		{
		}

		public override void Selected (DialogViewController dvc, MonoTouch.UIKit.UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			base.Selected (dvc, tableView, indexPath);

			if (OnSelected != null){
				OnSelected (this, null);
			}
		}

		public override UITableViewCell GetCell(UITableView tv)
		{
			var cell= base.GetCell(tv);
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;
			return cell;
		}
	}
}

