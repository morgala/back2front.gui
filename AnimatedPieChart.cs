﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using MonoTouch.CoreAnimation;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Back2Front.GUI
{
	public class AnimatedPieChart : UIView
	{
		private UIView _pieView;
		private NSTimer _animationTimer;
		private List<Slice> _slices;
		private List<SliceLayer> _sliceLayers;
		//private List<SliceLayer> _layersToRemove;
		private float _pieRadius;
		private PointF _pieCenter;
		private List<CAAnimation> _animations = new List<CAAnimation> ();
		private NSString _startAngleKey = new NSString("startAngle");
		private NSString _endAngleKey = new NSString("endAngle");
		private const float TIMEINTERVAL = 1.0f/60.0f;


		[DllImport (MonoTouch.Constants.CoreFoundationLibrary, CharSet=CharSet.Unicode)]
		internal extern static IntPtr CFRelease (IntPtr obj);

		public AnimatedPieChart(RectangleF frame)
		{
			_sliceLayers = new List<SliceLayer> ();
			//_layersToRemove = new List<SliceLayer> ();
			_slices = new List<Slice> ();

			_pieView = new UIView (frame);
			_pieView.BackgroundColor = UIColor.Clear;
			AddSubview (_pieView);

			AnimationSpeed = 0.5f;
			StartPieAngle = (float) Math.PI/2 * 3;
			SelectedSliceStroke = 3.0f;

			PieRadius = Math.Min (frame.Size.Width / 2, frame.Size.Height / 2) - 10;
			PieCenter = new PointF (frame.Size.Width / 2, frame.Size.Height / 2);
			SelectedSliceOffsetStroke = Math.Max (10, PieRadius / 10);
		}
			
		public float StartPieAngle{get;set;}
		public float AnimationSpeed{ get; set; }
		          
		public PointF PieCenter
		{
			get{ return _pieCenter; }
			set{
				_pieCenter = new PointF (_pieView.Frame.Size.Width/2, _pieView.Frame.Size.Height/2);
			}
		}

		public float PieRadius{
			get{ return _pieRadius; }
			set{ 
				_pieRadius = value;
//				PointF origin = _pieView.Frame.Location;
//				RectangleF frame = new RectangleF (origin.X + PieCenter.X - PieRadius,
//					origin.Y + PieCenter.Y  - PieRadius, PieRadius*2, PieRadius*2);
//				_pieCenter = new PointF (frame.Size.Width / 2, frame.Size.Height / 2);
//				_pieView.Frame = frame;
				_pieView.Layer.CornerRadius = _pieRadius;
			}
		}

		public bool ShowLabel{get;set;}
		public UIFont LabelFont{get;set;}
		public UIColor LabelColor{get;set;}
		public UIColor LabelShadowColor{ get; set; }
		public float LabelRadius{get;set;}
		public float SelectedSliceStroke{get;set;}
		public float SelectedSliceOffsetStroke{ get; set; }
		public bool ShowPercentage{ get; set; }
		public List<SliceLayer> SliceLayers{ get { return _sliceLayers; } }
		public List<Slice> Slices{ get { return _slices; }set{ _slices = value; } }
		public UIView PieView{
			get{ return _pieView; }
		}
			
		public void Load()
		{
			CALayer parentLayer = _pieView.Layer;
			double startToAngle = 0.0;
			double endToAngle = startToAngle;

			double sum = 0.0;
			int numOfSlices = Slices.Count;
			double[] values = new double[numOfSlices];
			for (int i = 0; i < numOfSlices; i++) {
				values [i] = Slices [i].Percentage;
				sum += values[i];
			}

			double[] angles = new double[numOfSlices];
			for (int i = 0; i < numOfSlices; i++) {
				double div;
				if (sum == 0)
					div = 0;
				else
					div = values[i] / sum; 
				angles[i] = Math.PI * 2 * div;
			}

			CATransaction.Begin ();
			CATransaction.AnimationDuration = AnimationSpeed;
			_pieView.UserInteractionEnabled = false;

			for (int i = 0; i < numOfSlices; i++) {
				SliceLayer layer = null;
				double angle = angles[i];
				endToAngle += angle;
				double startFromAngle = StartPieAngle + startToAngle;
				double endFromAngle = StartPieAngle + endToAngle;

				layer = CreateSliceLayer ();
				startFromAngle = endFromAngle = StartPieAngle;
				parentLayer.AddSublayer (layer);
				SliceLayers.Add (layer);

				layer.Value = (float)values[i];
				layer.Percentage = (float)(layer.Value / sum);
				layer.FillColor = Slices [i].Color;
				layer.CreateArcAnimationForKey ("startAngle", (float)startFromAngle, StartPieAngle + (float)startToAngle, AnimationStarted, AnimationStopped);
				layer.CreateArcAnimationForKey ("endAngle", (float)endFromAngle, (float)endToAngle + StartPieAngle, AnimationStarted, AnimationStopped);
				startToAngle = endToAngle;
			}
				
			_pieView.UserInteractionEnabled = true;
			CATransaction.Commit ();
		}

		public void Clear()
		{
			foreach (SliceLayer layer in SliceLayers) {
				layer.RemoveFromSuperLayer ();
			}
			SliceLayers.Clear ();
		}

		private SliceLayer CreateSliceLayer()
		{
			var pieLayer = new SliceLayer ();
			pieLayer.ZPosition = 0;
			pieLayer.StrokeColor = null;
			return pieLayer;
		}


		private void AnimationStarted(CAAnimation anim)
		{
			if (_animationTimer == null) {
				_animationTimer = NSTimer.CreateRepeatingScheduledTimer (TIMEINTERVAL, () => UpdateTimerFired());
				NSRunLoop.Current.AddTimer (_animationTimer, NSRunLoopMode.Common);
			}
			_animations.Add (anim);
		}

		private void AnimationStopped(CAAnimation anim, bool finished)
		{
			_animations.Remove (anim);
			if (_animations.Count == 0) {
				_animationTimer.Invalidate ();
				_animationTimer = null;
			}
		}

		private void UpdateTimerFired()
		{
			if (_pieView.Layer.Sublayers == null) {
				return;
			}
			foreach (SliceLayer layer in _pieView.Layer.Sublayers) {
				NSObject presentationLayerStartAngle = layer.PresentationLayer.ValueForKey (_startAngleKey);
				float interpolatedStartAngle = ((NSNumber)presentationLayerStartAngle).FloatValue;

				NSObject presentationLayerEndAngle = layer.PresentationLayer.ValueForKey (_endAngleKey);
				float interpolatedEndAngle = ((NSNumber)presentationLayerEndAngle).FloatValue;

				CGPath path = CreatePathArc (PieCenter, PieRadius, interpolatedStartAngle, interpolatedEndAngle);
				layer.Path = path;
				//CFRelease (path.Handle);
			}
		}

		private void DeselectSlice(SliceLayer slice)
		{
			slice.Position = PointF.Empty;
			slice.IsSelected = false;
		}

		public static CGPath CreatePathArc(PointF center, float radius, float startAngle, float endAngle)
		{
			CGPath path = new CGPath ();
			path.MoveToPoint (center.X, center.Y);
			path.AddArc (center.X, center.Y, radius, startAngle, endAngle, false);
			path.CloseSubpath ();
			return path;
		}

	}
}

