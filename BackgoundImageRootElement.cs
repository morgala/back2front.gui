using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Back2Front.GUI
{
	public class BackgoundImageRootElement : RootElement
	{
		private readonly NSString _cellKey = new NSString("Root");
		private UIImage _backImage;
		private UIColor _cellBackgroundColor;
		private UIColor _textColor;


		public UIImage BackgroundImage
		{
			get{ return _backImage; }
			set{ _backImage = value; }
		}

		public string CellText{ get; set; }

		public BackgoundImageRootElement(string caption, int section, int element, UIImage backgroundImage, UIColor cellBackgroundColor, UIColor textColor) : base(caption, section, element)
		{

			_backImage = backgroundImage;
			_cellBackgroundColor = cellBackgroundColor;
			_textColor = textColor;
		}
		
		public BackgoundImageRootElement(string caption, UIImage backgroundImage, UIColor cellBackgroundColor, UIColor textColor) :
					this(caption, backgroundImage, cellBackgroundColor, textColor, null)
		{
		}

		public BackgoundImageRootElement(string caption, UIImage backgroundImage, UIColor cellBackgroundColor, UIColor textColor, Group group) : base(caption, group)
		{
			_backImage = backgroundImage;
			_cellBackgroundColor = cellBackgroundColor;
			_textColor = textColor;
		}

		public BackgoundImageRootElement(string caption, Group group) : base(caption, group)
		{
		}

		protected override MonoTouch.Foundation.NSString CellKey {
			get
			{
				return _cellKey;
			}	
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			var cell = base.GetCell (tv);
			cell.BackgroundColor = _cellBackgroundColor;
			cell.TextLabel.TextColor = _textColor;
			cell.DetailTextLabel.TextColor = _textColor;
			cell.SelectionStyle = UITableViewCellSelectionStyle.Gray;

			if (!string.IsNullOrEmpty (CellText)) {
				cell.DetailTextLabel.Text = CellText;
			}
			return cell;
		}

		protected override UIViewController MakeViewController ()
		{
			var vc = (DialogViewController)base.MakeViewController();
			vc.TableView.BackgroundView = null;
			//var background = UIImage.FromFile ("appbg.png");
			vc.TableView.BackgroundView = new UIImageView(_backImage);
			return vc;
		}
	}
}

