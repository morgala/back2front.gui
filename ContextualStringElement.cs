using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Back2Front.GUI
{
	public class ContextualStringElement : StyledStringElement
	{
		private readonly NSString _cellKey = new NSString("ContextualStringElement");
		private bool _showAccessory;
		private UITableViewCellSelectionStyle _selectionStyle = UITableViewCellSelectionStyle.Gray;
		private UIColor _textColor;

		public ContextualStringElement (string caption, string value) : this(caption, value, UIColor.Black, UIColor.White)
		{

		}

		public ContextualStringElement (string caption, string value, UIColor textColor, UIColor backgroundColor) : base(caption, value)
		{
			_textColor = textColor;
			BackgroundColor = backgroundColor;
		}

		public ContextualStringElement (string caption) : base(caption)
		{

		}

		public ContextualStringElement(string caption, UITableViewCellSelectionStyle style):base(caption)
		{
			_selectionStyle = style;
		}

		public ContextualStringElement(string caption, NSAction onTap)
			: base(caption, onTap)
		{

		}

		protected override NSString CellKey {
			get {
				return _cellKey;
			}
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			var cell = base.GetCell (tv);		
			cell.SelectionStyle = _selectionStyle;	
			cell.TextLabel.TextColor = _textColor;
			cell.DetailTextLabel.TextColor = _textColor;
			cell.DetailTextLabel.Font = UIFont.SystemFontOfSize (17);
			cell.DetailTextLabel.TextAlignment = UITextAlignment.Left;
			if (_showAccessory) {
				cell.Accessory = UITableViewCellAccessory.Checkmark;
			}
			return cell;
		}	

		public void SetValueAndUpdate(string value, bool showAccessory)
		{
			_showAccessory = showAccessory;
			Value = value;
			if(GetContainerTableView () != null)
			{
				var root = GetImmediateRootElement();
				root.Reload (this, UITableViewRowAnimation.Fade);
			}
		}

		public void SetValueAndUpdate(string value)
		{
			SetValueAndUpdate (value, false);
		}
	}
}

