using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Back2Front.GUI
{
	public class SelectedStringElement : StringElement
	{
		private UITableViewCellSelectionStyle _selectionStyle;
		private readonly NSString _cellKey = new NSString("SelectedStringElement");
		private UIColor _textColor;
		private UIColor _backgroundColor;

		public SelectedStringElement (string caption, UITableViewCellSelectionStyle selectionStyle,  NSAction action, UIColor textColor, UIColor backgroundColor) : base(caption, action)
		{
			_selectionStyle = selectionStyle;
			_textColor = textColor;
			_backgroundColor = backgroundColor;
		}

		protected override NSString CellKey {
			get {
				return _cellKey;
			}
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			var cell = base.GetCell (tv);
			cell.SelectionStyle = _selectionStyle;
			cell.BackgroundColor = _backgroundColor;
			cell.TextLabel.TextColor = _textColor;
			return cell;
		}
	}
}

