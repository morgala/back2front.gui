using System;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace Back2Front.GUI
{
	public class BooleanElementAdv : BooleanElement
	{
		private UIColor _backgroundColor;
		private UIColor _textColor;


		public BooleanElementAdv(string caption, bool value, UIColor textColor,  UIColor backgroundColor) : base(caption,value)
		{
			_backgroundColor = backgroundColor;
			_textColor = textColor;
		}

		public override UITableViewCell GetCell(UITableView tv)
		{
			var cell= base.GetCell(tv);
			cell.BackgroundColor = _backgroundColor;
			cell.TextLabel.TextColor = _textColor;
			((UILabel)cell.ContentView.Subviews [0]).TextColor = _textColor;

			return cell;
		}
	}
}

